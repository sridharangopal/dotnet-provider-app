using System;
using System.Collections.Generic;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using PactNet;
using PactNet.Infrastructure.Outputters;
using tests.XUnitHelpers;
using Xunit;
using Xunit.Abstractions;

namespace tests
{
    public class ProviderApiTests : IDisposable
    {
        private string _providerUri { get; }
        private string _pactServiceUri { get; }
        private IWebHost _webHost { get; }
        private ITestOutputHelper _outputHelper { get; }

        private PactVerifierConfig config;

        public ProviderApiTests(ITestOutputHelper output)
        {
            _outputHelper = output;
            _providerUri = "http://localhost:9000";
            _pactServiceUri = "http://localhost:9001";

            this.config = new PactVerifierConfig
            {
                Outputters = new List<IOutput>{
                    new XUnitOutput(_outputHelper)
                },
                Verbose = false
            };

            _webHost = WebHost.CreateDefaultBuilder()
                .UseUrls(_pactServiceUri)
                .UseStartup<TestStartup>()
                .Build();

            _webHost.Start();
        }

        [Fact]
        public void EnsureProviderApiHonoursPactWithDotnetConsumer()
        {

            //Act / Assert
            IPactVerifier pactVerifier = new PactVerifier(this.config);
            pactVerifier.ProviderState($"{_pactServiceUri}/provider-states")
                .ServiceProvider("Dotnet-Provider", _providerUri)
                .HonoursPactWith("Dotnet-Consumer")
                .PactUri(@"https://gitlab.com/sridharangopal/dotnet-consumer-app/-/raw/master/pacts/dotnet-consumer-dotnet-provider.json")
                .Verify();
        }

        [Fact]
        public void EnsureProviderApiHonoursPactWithJavaConsumer()
        {

            //Act / Assert
            IPactVerifier pactVerifier = new PactVerifier(this.config);
            pactVerifier.ProviderState($"{_pactServiceUri}/provider-states")
                .ServiceProvider("Dotnet-Provider", _providerUri)
                .HonoursPactWith("Java-Consumer")
                .PactUri(@"https://gitlab.com/sridharangopal/java-consumer-app/-/raw/master/src/pacts/java-consumer-dotnet-provider.json")
                .Verify();

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _webHost.StopAsync().GetAwaiter().GetResult();
                    _webHost.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
